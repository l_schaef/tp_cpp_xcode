#include "GrilleMorpion.hpp"
#include <iostream>

GrilleMorpion::GrilleMorpion(){
    for (int i = 0; i < NB_LINE; i++) {
        std::vector<Case> tmp;
        for (int j = 0; j < NB_ROW; j++) {
            tmp.push_back(Case(i, j));
        }
        m_Grille.push_back(tmp);
    }
}

// Override Grille

void GrilleMorpion::reglement(){
    std::cout << "          Bienvenue dans le jeu du Morpion !" << std::endl;
    std::cout << std::endl;
    std::cout << "Voici les règles : " << std::endl;
    std::cout << "Le but du jeu est d’aligner avant son adversaire 3 symboles ( 1 ou 2 ) identiques" << std::endl;
    std::cout << "horizontalement, verticalement ou en diagonale dans une grille de 3 lignes et de 3 colones." << std::endl;
    std::cout << "Le premier des deux joueurs qui réussi à aligner 3 de ses symboles gagne la partie." << std::endl;
    std::cout << std::endl;
    std::cout << "Pour éviter toute incompréhension voici comment jouer : " << std::endl;
    std::cout << "Il faudra saisir chacun votre tour 2 numéros séparé par un espace (ligne et colone)" << std::endl;
    std::cout << "Exemple de saisie : 2 3" << std::endl;
    std::cout << "En cas de saisie non valable la saisie sera redemandée" << std::endl;
    std::cout << std::endl;
    std::cout << "Voici les données à saisir pour les cases : " << std::endl;
    std::cout << std::endl;
    std::cout << "1 1 | 1 2 | 1 3" << std::endl;
    std::cout << "2 1 | 2 2 | 2 3" << std::endl;
    std::cout << "3 1 | 3 2 | 3 3" << std::endl;
    std::cout << std::endl;
    std::cout << "Bon jeu !" << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
}

void GrilleMorpion::start(){
    int newPlay;
    reglement();
    int currentPlayer = 0;
    do{
        nextPlayer(currentPlayer);
        print();
        std::cout << std::endl;
        Case entryCase = saisiePlayer(currentPlayer);
        placeAToken(entryCase, currentPlayer);
        std::cout << std::endl;
    } while(!(gameWinner(currentPlayer)) && !(grilleFull()));

    if (gameWinner(currentPlayer)) {
        std::cout << "Bravo joueur " << currentPlayer << " tu as gagné" << std::endl;
    } else if (grilleFull()) {
        std::cout << "Match Nul" << std::endl;
        std::cout << "Voulez - vous rejouer ?" << std::endl;
        std::cout << "1 - Oui" << std::endl;
        std::cout << "2 - Non" << std::endl;
        while(!(std::cin >> newPlay)|| newPlay < 1 || newPlay > 3){
            if(std::cin.fail()){
                std::cout << "Erreur de saisie. Re-saisir : " << std::endl;
                std::cin.clear();
                std::cin.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
            } else if (newPlay < 1 || newPlay > 3) {
                std::cout << "Erreur de saisie. Re-saisir entre 1 et 2 : " << std::endl;
            }
        }
        if(newPlay == 1){
            reset();
            start();
        } else {
            std::cout << "Au revoir ! A bientôt !" << std::endl;
        }
    }
}

Case GrilleMorpion::saisiePlayer(int& player){
    int x, y;
    std::cout << "C'est à toi joueur " << player << " : " << std::endl;
    std::cout << "Ou veux tu placer ton pion ?" << std::endl;
    while(!(std::cin >> x >> y) || x < 1 || x > 4 || y < 1 || y > 4 || !(emptyCase(Case(x, y)))){
        if(std::cin.fail()){
            std::cout << "Erreur de saisie. Re-saisir : " << std::endl;
            std::cin.clear();
            std::cin.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
        } else if (x < 1 || x > 4 || y < 1 || y > 4) {
            std::cout << "Erreur de saisie. Re-saisir entre 1 et 3 : " << std::endl;
        } else if (!(emptyCase(Case(x, y)))) {
            std::cout << "La case est déjà utilisée. Re-saisir une nouvelle case : " << std::endl;
        }
    }
    return Case(x, y);
}

void GrilleMorpion::placeAToken(Case c, int& idJoueur){
    m_Grille[c.getX()-1][c.getY()-1].Val = idJoueur;
}

bool GrilleMorpion::gameWinner(int& idJoueur){
    for (int i = 0; i < 3; i++) {
        if(completeLine(i, idJoueur)){
            return true;
        }
        if (completeRow(i, idJoueur)) {
            return true;
        }
    }
    if(completeDiagonal(0, idJoueur) || completeDiagonal(1, idJoueur)){
        return true;
    }
    return false;
}

// Override CheckingAlignment

bool GrilleMorpion::completeLine(int idLine, int& idPlayer){
    for (int i = 0; i < NB_ROW; i++){
        if(m_Grille[idLine-1][i].Val != idPlayer)
            return false;
    }
    return true;
};

bool GrilleMorpion::completeRow(int idRow, int& idPlayer){
    for (int i = 0; i < NB_LINE; i++){
        if(m_Grille[i][idRow-1].Val != idPlayer)
            return false;
    }
    return true;
}

bool GrilleMorpion::completeDiagonal(int idDiag, int& idPlayer){
    if (idDiag == 0){
        for (int i = 0; i < 3; i++){
            if (m_Grille[i][i].Val != idPlayer)
                return false;
        }
    } else if (idDiag == 1){
        int j = NB_ROW - 1;
        for (int i = 0; i < NB_LINE; i++){
            if (m_Grille[i][j].Val != idPlayer)
                return false;
            j--;
        }
    }
    else {
        return false;
    }
    return true;
}


bool GrilleMorpion::grilleFull(){
    for (int i = 0; i < m_Grille.size(); i++) {
        for (int j = 0; j < m_Grille[i].size(); j++) {
            if(m_Grille[i][j].Val == VIDE){
                return false;
            }
        }
    }
    return true;
}

