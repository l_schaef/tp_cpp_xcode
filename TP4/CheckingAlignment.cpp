//
//  CheckingAlignment.cpp
//  TP4
//
//  Created by Laura Schaefer on 08/11/2020.
//
#include "CheckingAlignment.hpp"
#include <stdio.h>


bool CheckingAlignment::grilleFull(){
    for (int i = 0; i < m_Grille.size(); i++) {
        for (int j = 0; j < m_Grille[i].size(); j++) {
            if(m_Grille[i][j].Val == VIDE){
                return false;
            }
        }
    }
    return true;
}
