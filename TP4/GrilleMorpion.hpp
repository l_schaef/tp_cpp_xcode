//
//  GrilleMorpion.hpp
//  CPP
//
//  Created by Thomas Durst on 14/10/2020.
//  Copyright © 2020 Thomas Durst. All rights reserved.
//

#ifndef GrilleMorpion_hpp
#define GrilleMorpion_hpp

#include <stdio.h>
#include <vector>
#include "Grille.hpp"
#include "CheckingAlignment.hpp"
#include "Case.hpp"

#define NB_LINE 3
#define NB_ROW 3

class GrilleMorpion : public Grille , public CheckingAlignment {
    
public:
    GrilleMorpion();
    // Override CheckingAlignment
    
    bool completeLine(int idLine, int& idPlayer) override;
    bool completeRow(int idRow, int& idPlayer) override;
    bool completeDiagonal(int idDiagonal, int& idPlayer) override;
    bool grilleFull() override;
    
    // Override Grille

    void start() override;
    void reglement() override;
    void placeAToken(Case c, int& idJoueur) override;
    bool gameWinner(int& idJoueur) override;
    Case saisiePlayer(int& idJoueur) override;
};

#endif /* GrilleMorpion_hpp */
