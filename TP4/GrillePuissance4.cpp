//
//  GrillePuissance4.cpp
//  CPP
//
//  Created by Thomas Durst on 18/10/2020.
//  Copyright © 2020 Thomas Durst. All rights reserved.
//

#include "GrillePuissance4.hpp"

GrillePuissance4::GrillePuissance4(){
    for (int i = 0; i < NB_LINE; i++) {
        std::vector<Case> tmp;
        for (int j = 0; j < NB_ROW; j++) {
            tmp.push_back(Case(i, j));
        }
        m_Grille.push_back(tmp);
    }
}

void GrillePuissance4::placeAToken(Case c, int& idJoueur){
    for (int i = NB_LINE - 1; i > -1; i--) {
        if(m_Grille[i][c.getY()-1].Val == VIDE){
            m_Grille[i][c.getY()-1].Val = idJoueur;
            break;
        }
    }
}

//bool GrillePuissance4::emptyCase(int idCol){
//    for (int i = NB_LINE - 1; i > -1; i--) {
//        if(m_Grille[i][idCol-1].Val == VIDE){
//            return true;
//            break;
//        }
//    }
//    return false;
//}

bool GrillePuissance4::completeRow(int idCol, int& idPlayer){
    for (int i = NB_LINE-1; i > -1; i--) {
        if(idPlayer != m_Grille[i][idCol-1].Val){
            return false;
            break;
        }
    }
    return true;
}

bool GrillePuissance4::completeLine(int idLine, int& idPlayer){
    bool isOK = false;
    int compteur = 0;
    for (int i = 0; i < NB_ROW; i++) {
        if(idPlayer == m_Grille[idLine - 1][i].Val){
            compteur++;
            isOK = true;
        } else {
            compteur = 0;
            isOK = false;
        }
        if(isOK && compteur == 4){
            break;
        }
    }
    return isOK;
}

bool GrillePuissance4::completeDiagonal(int idDiag, int& idPlayer){
    int y = 0;
    bool isOK = false;
    if(idDiag == 0){
        for (int j = 0; j < NB_DIAGONALE; j++) {
            y = 0;
            for(int i = 0; i < NB_DIAGONALE; i++) {
                if(idPlayer == m_Grille[i][y].Val){
                    isOK = true;
                } else {
                    isOK = false;
                    break;
                }
                y++;
            }
            // Il a déjà vérifié et elle est valable break pour gagner du temps
            if(isOK){
                return isOK;
                break;
            }
        }
        return isOK;
    } else if(idDiag == 1){
        for (int j = NB_DIAGONALE - 1; j > -1; j--) {
            y = 0;
            for(int i = NB_DIAGONALE - 1; i > -1; i--) {
                if(idPlayer == m_Grille[i][y].Val){
                    isOK = true;
                } else {
                    isOK = false;
                    break;
                }
                y++;
            }
            // Il a déjà vérifié et elle est valable break pour gagner du temps
            if(isOK){
                return isOK;
                break;
            }
        }
        return isOK;
    } else {
        return false;
    }
}

bool GrillePuissance4::gameWinner(int& idJoueur){
    for (int i = 1; i < 5; i++) {
        if(completeLine(i, idJoueur)){
            return true;
            break;
        }
    }
    for (int i = 1; i < 8; i++) {
        if(completeRow(i, idJoueur)){
            return true;
            break;
        }
    }
    if(completeLine(0, idJoueur) || completeLine(1, idJoueur)){
        return true;
    }
    return false;
}

void GrillePuissance4::start(){
    int saisieDuJoueurMenuDuJeu;
    reglement();
    int tourPlayer = 1;
    do{
        print();
        std::cout << std::endl;
        placeAToken(saisiePlayer(tourPlayer), tourPlayer);
        nextPlayer(tourPlayer);
        std::cout << std::endl;
    } while(!(gameWinner(1)) && !(gameWinner(2)) && !(grilleFull()));
    
    if (gameWinner(1)) {
        std::cout << "Bravo joueur 1 tu as gagné" << std::endl;
    } else if (gameWinner(2)) {
        std::cout << "Bravo joueur 2 tu as gagné" << std::endl;
    } else if (grilleFull()) {
        std::cout << "Match Nul" << std::endl;
    }
    std::cout << "Voulez - vous rejouer ?" << std::endl;
    std::cout << "1 - Oui" << std::endl;
    std::cout << "2 - Non" << std::endl;
    while(!(std::cin >> saisieDuJoueurMenuDuJeu)|| saisieDuJoueurMenuDuJeu < 0 || saisieDuJoueurMenuDuJeu > 3){
        if(std::cin.fail()){
            std::cout << "Erreur de saisie. Re-saisir : " << std::endl;
            std::cin.clear();
            std::cin.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
        } else if (saisieDuJoueurMenuDuJeu < 0 || saisieDuJoueurMenuDuJeu > 3) {
            std::cout << "Erreur de saisie. Re-saisir entre 1 et 2 : " << std::endl;
        }
    }
    if(saisieDuJoueurMenuDuJeu == 1){
        reset();
        start();
    }
}

void GrillePuissance4::reglement(){
    std::cout << "          Bienvenue dans le jeu du Puissance 4 !" << std::endl;
    std::cout << std::endl;
    std::cout << "Voici les régles : " << std::endl;
    std::cout << "Le but du jeu est d’aligner avant son adversaire 4 symboles ( 1 ou 2 ) de façon identiques" << std::endl;
    std::cout << "horizontalement, verticalement ou en diagonale dans une grille de 4 lignes et de 7 colones." << std::endl;
    std::cout << "Le premier des deux joueurs qui réussi à aligner son symbole gagne la partie." << std::endl;
    std::cout << std::endl;
    std::cout << "Comment jouer : " << std::endl;
    std::cout << "Pour éviter toute incompréhension voici comment jouer." << std::endl;
    std::cout << "Il faudra saisir chacun votre tour 1 numéro qui correspond à la colone" << std::endl;
    std::cout << "Exemple de saisie : 2" << std::endl;
    std::cout << "En cas de saisie non valable la saisie serra redemandé" << std::endl;
    std::cout << std::endl;
    std::cout << "Voici les données à saisir pour les cases : " << std::endl;
    std::cout << std::endl;
    std::cout << "1 | 2 | 3 | 4 | 5 | 6 | 7" << std::endl;
    std::cout << std::endl;
    std::cout << "Bon jeu !" << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
}

Case GrillePuissance4::saisiePlayer(int& player){
    int saisieDeLaColonneOuPlacerLePion;
    std::cout << "C'est à toi joueur " << player << " : " << std::endl;
    std::cout << "Ou veux tu placer ton pion ?" << std::endl;
    while(!(std::cin >> saisieDeLaColonneOuPlacerLePion) || saisieDeLaColonneOuPlacerLePion < 1 || saisieDeLaColonneOuPlacerLePion > 8 || !(emptyCase())){
        if(std::cin.fail()){
            std::cout << "Erreur de saisie. Re-saisir : " << std::endl;
            std::cin.clear();
            std::cin.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
        } else if (saisieDeLaColonneOuPlacerLePion < 1 || saisieDeLaColonneOuPlacerLePion > 8) {
            std::cout << "Erreur de saisie. Re-saisir entre 1 et 7 : " << std::endl;
        } else if (!(emptyCase(x))) {
            std::cout << "Erreur de saisie. La colone n'a plus de place" << std::endl;
        }
    }
    return x;
}

Case GrillePuissance4::getFirstCaseEmpty(int idRow){
    for (int i = NB_LINE - 1; i > -1; i--) {
            if(m_Grille[i][idRow-1].Val == VIDE){
                return Case(i, idRow-1);
           }
    }
    return Case(0, idRow);
}
