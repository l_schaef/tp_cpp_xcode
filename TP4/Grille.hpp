//
//  Grille.hpp
//  CPP
//
//  Created by Thomas Durst on 20/10/2020.
//  Copyright © 2020 Thomas Durst. All rights reserved.
//

#ifndef Grille_hpp
#define Grille_hpp

#include <stdio.h>
#include <vector>
#include "Case.hpp"
#include <iostream>

#define PLAYER_1 1
#define PLAYER_2 2
#define VIDE 0

class Grille {
    public:
        bool emptyCase(Case c);
        void reset();
        void nextPlayer(int& playerCourant);
        void print();
    
        virtual void start() = 0;
        virtual void reglement() = 0;
        virtual void placeAToken(Case c, int& idJoueur) = 0;
        virtual bool gameWinner(int& idJoueur) = 0;
        virtual Case saisiePlayer(int& idJoueur) = 0;
    protected:
        std::vector<std::vector<Case>> m_Grille;
};
#endif /* Grille_hpp */
