//
//  Grille.cpp
//  CPP
//
//  Created by Thomas Durst on 20/10/2020.
//  Copyright © 2020 Thomas Durst. All rights reserved.
//

#include "Grille.hpp"

void Grille::print(){
    for (int i = 0; i < m_Grille.size(); i++) {
        for (int j = 0; j < m_Grille[i].size(); j++) {
            std::cout <<  m_Grille[i][j].Val << " ";
        }
        std::cout << std::endl;
    }
}

void Grille::reset(){
    for (int i = 0; i < m_Grille.size(); i++) {
        for (int j = 0; j < m_Grille[i].size(); j++) {
            m_Grille[i][j].Val = VIDE;
        }
    }
}

bool Grille::emptyCase(Case c){
    return (m_Grille[c.getX()-1][c.getY()-1].Val == VIDE);
}

void Grille::nextPlayer(int& PlayerCourant){
    if(PlayerCourant == 0){ //Initialisation des joueurs à 0
        PlayerCourant = PLAYER_1;
    } else if(PlayerCourant == PLAYER_1){
        PlayerCourant = PLAYER_2;
    } else if(PlayerCourant == PLAYER_2) {
        PlayerCourant = PLAYER_1;
    }
}
