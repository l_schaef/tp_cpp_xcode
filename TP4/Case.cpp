//
//  Case.cpp
//  CPP
//
//  Created by Thomas Durst on 14/10/2020.
//  Copyright © 2020 Thomas Durst. All rights reserved.
//

#include "Case.hpp"

Case::Case(int _X, int _Y){
    m_X = _X;
    m_Y = _Y;
    Val = VIDE;
}

int Case::getX(){
    return m_X;
}

int Case::getY(){
    return m_Y;
}
