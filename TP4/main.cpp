//
//  main.cpp
//  CPP
//
//  Created by Thomas Durst on 15/10/2020.
//  Copyright © 2020 Thomas Durst. All rights reserved.
//

#include <stdio.h>
#include "Game.hpp"
#include "GrilleMorpion.hpp"
#include "GrillePuissance4.hpp"

int main(int argc, const char * argv[]) {
    Game g(new GrillePuissance4());
    return 0;
}
