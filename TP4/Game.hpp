//
//  Game.hpp
//  CPP
//
//  Created by Thomas Durst on 17/10/2020.
//  Copyright © 2020 Thomas Durst. All rights reserved.
//

#ifndef Game_hpp
#define Game_hpp

#include <stdio.h>
#include <iostream>
#include "Grille.hpp"

class Game{
    public:
        Game(Grille* grille);
};
#endif /* Game_hpp */
