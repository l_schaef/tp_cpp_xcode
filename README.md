Amélioration du TP3 afin de respecter les principes SOLID et la loi de Demeter
Binôme : Thomas DURST et Laura SCHAEFER

TP4 basé sur le TP3 de Thomas DURST pour les raisons suivantes :
Classe filles GrilleMorpion et GrillePuissance4 héritant de la classe mère Grille
Plusieurs petites méthodes (SaisieJoueur, NextPlayer, Reglement, Game) plutôt qu’une méthode plus lourde à maintenir (Jeu) 

S : Single responsibility principle

La classe Grille fait jouer une partie d’un jeu sur plateau.
La classe CheckingAlignment vérifie l’alignement des pions.
Les classes GrilleMorpion et GrillePuissance4 permettent de jouer aux jeux Morpion et Puissance 4.

O : Open/Close principle

Les classes Grille et CheckingAlignment sont étendues par les classes GrilleMorpion et GrillePuissance4.

L : Liskov substitution principle

Toutes les méthodes des classes mères (Grille et CheckingAlignment) sont utilisées et utiles dans les classes GrilleMorpion et GrillePuissance4.

I : Interface segregation principle

La classe Game ne voit pas ce qu’il se passe quand on démarre le jeu.
GrilleMorpion et GrillePuissance4 héritent de Grille et de CheckingAlignment car un jeu sous forme de grille n’a pas forcément pour objectif d’aligner des jetons.

D : Dependency inversion principle

Le jeu prend en paramètres une Grille, que ce soit une GrilleMorpion ou une GrillePuissance4.

Grille* grilleMorpion = new GrilleMorpion();
Grille* grillePuissance4  = new GrillePuissance4();
Game game(grillePuissance4);

Loi de Demeter

La loi de Demeter était déjà respectée dans le TP3.
Chaque méthode invoque uniquement les méthodes et les propriétés de l’objet lui-même.
