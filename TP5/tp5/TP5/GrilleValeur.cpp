//
//  GrilleValeur.cpp
//  tp5
//
//  Created by Thomas Durst on 17/11/2020.
//

#include "GrilleValeur.hpp"

void GrilleValeur::reset(){
    for (int i = 0; i < m_Grille.size(); i++) {
        for (int j = 0; j < m_Grille[i].size(); j++) {
            m_Grille[i][j].Val = VIDE;
        }
    }
}

bool GrilleValeur::emptyCase(Case c){
    return (m_Grille[c.getX()-1][c.getY()-1].Val == VIDE);
}
