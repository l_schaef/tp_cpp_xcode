//
//  Game.cpp
//  CPP
//
//  Created by Thomas Durst on 17/10/2020.
//  Copyright © 2020 Thomas Durst. All rights reserved.
//

#include "Game.hpp"
#include "GrilleDames.hpp"
#include "GrilleMorpion.hpp"
#include "GrillePuissance4.hpp"

Game::Game(){
    int newPlay = 0;
    std::cout << "Match Nul" << std::endl;
    std::cout << "Que voulez vous faire ?" << std::endl;
    std::cout << "1 - Jeu de Dame" << std::endl;
    std::cout << "2 - Jeu du Morpion" << std::endl;
    std::cout << "3 - Jeu du Puissance 4" << std::endl;
    while(!(std::cin >> newPlay)|| newPlay < 1 || newPlay > 4){
        if(std::cin.fail()){
            std::cout << "Erreur de saisie. Re-saisir : " << std::endl;
            std::cin.clear();
            std::cin.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
        } else if (newPlay < 1 || newPlay > 4) {
            std::cout << "Erreur de saisie. Re-saisir entre 1 et 3 : " << std::endl;
        }
    }
    switch (newPlay) {
        case 1:
            GrilleDames().start();
            break;
        case 2:
            GrilleMorpion().start();
            break;
        case 3:
            GrillePuissance4().start();
            break;
    }
}
