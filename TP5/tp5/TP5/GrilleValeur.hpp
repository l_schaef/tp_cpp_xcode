//
//  GrilleValeur.hpp
//  tp5
//
//  Created by Thomas Durst on 17/11/2020.
//

#ifndef GrilleValeur_hpp
#define GrilleValeur_hpp

#include <stdio.h>
#include <vector>
#include "Grille.hpp"
#include "CaseWithValue.hpp"

class GrilleValeur : public Grille {
    public:
        bool emptyCase(Case c);
        void reset() override;
        virtual void placeAToken(Case c, int& idJoueur) = 0;
    protected:
        std::vector<std::vector<CaseWithValue>> m_Grille;
};

#endif /* GrilleValeur_hpp */
