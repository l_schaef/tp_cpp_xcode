//
//  ColorCase.hpp
//  TP4
//
//  Created by Laura Schaefer on 15/11/2020.
//

#ifndef ColorCase_hpp
#define ColorCase_hpp

#include <stdio.h>
#include <iostream>
#include "Case.hpp"
#include "Piece.hpp"
#include "EnumCase.hpp"

class CaseWithColor: public Case {
    public:
        CaseWithColor(int _X, int _Y, ECouleurCase couleurCase);
        ECouleurCase Color;
        Piece* pieceOnCase;
};

#endif /* ColorCase_hpp */
