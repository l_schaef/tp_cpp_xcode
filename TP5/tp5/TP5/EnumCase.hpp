//
//  EnumCouleur.hpp
//  TP4
//
//  Created by Laura Schaefer on 15/11/2020.
//

#ifndef EnumCouleur_hpp
#define EnumCouleur_hpp

#include <stdio.h>

enum ECouleurCase {Blanche, Noire};

#endif /* EnumCouleur_hpp */
