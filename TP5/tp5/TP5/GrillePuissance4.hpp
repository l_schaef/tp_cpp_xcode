//
//  GrillePuissance4.hpp
//  CPP
//
//  Created by Thomas Durst on 18/10/2020.
//  Copyright © 2020 Thomas Durst. All rights reserved.
//

#ifndef GrillePuissance4_hpp
#define GrillePuissance4_hpp

#include <stdio.h>
#include <vector>
#include "Case.hpp"
#include "Grille.hpp"
#include "CheckingAlignment.hpp"
#include "CaseWithValue.hpp"
#include "GrilleValeur.hpp"
#include <iostream>

#define NB_LINE 4
#define NB_ROW 7
#define NB_DIAGONALE 4 // 4 diagonale dans un puissance 4 | 4 dans un sens 4 dans l'autre

class GrillePuissance4 : public GrilleValeur, CheckingAlignment {
    public:
        GrillePuissance4();
    
        // Override class ChekcingAlignment
        bool completeLine(int IdLine, int& idPlayer) override;
        bool completeRow(int IdCol, int& idPlayer) override;
        bool completeDiagonal(int IdDiag, int& idPlayer) override;
        bool grilleFull() override;
    
        // Override class Grille
        void print() override;
        void start() override;
        void placeAToken(Case c, int& idJoueur) override;
        void reglement() override;
        bool gameWinner(int& idJoueur) override;
        Case saisiePlayer(int& player) override;
    
        Case getFirstCaseEmpty(int idRow);
        bool placeInTheRow(int idRow);
};

#endif /* GrillePuissance4_hpp */
