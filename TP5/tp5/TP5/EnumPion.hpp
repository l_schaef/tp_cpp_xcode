//
//  EnumPion.hpp
//  TP4
//
//  Created by Laura Schaefer on 15/11/2020.
//

#ifndef EnumPion_hpp
#define EnumPion_hpp

#include <stdio.h>

enum ECouleurPion {Blanc, Noir, Vide};
enum ETypePion {Pion, Dame};

#endif /* EnumPion_hpp */
