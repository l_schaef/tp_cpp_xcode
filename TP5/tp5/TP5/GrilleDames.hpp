//
//  GrilleDames.hpp
//  TP4
//
//  Created by Thomas Durst on 15/11/2020.
//

#ifndef GrilleDames_hpp
#define GrilleDames_hpp

#include <stdio.h>
#include "GrilleCouleur.hpp"
#include "CaseWithColor.hpp"
#include "EnumCase.cpp"

#define NB_LINE 10
#define NB_ROW 10

class GrilleDames :  public GrilleCouleur {
public:
    GrilleDames();
    void initialisePieces();
    void moveAPion(Case originCase, Case caseToPlacePion);
    char showTypePiece(Piece* piece);
    void takePiece(Case pionTaken);
    int maxPrisesOfPiece(CaseWithColor caseOrigin, ECouleurPion color);
    int priseDispo(CaseWithColor originCase);
    std::vector<Case> caseAfterPrises(CaseWithColor originCase);
    bool coup_obligatoire(ECouleurPion color);
    int size(std::vector<Case> list);
    std::vector<Case> caseObligatoire(ECouleurPion color);
    ECouleurPion couleurJoueur(int& IdJoueur);
    ECouleurPion couleurPionCase(Case casePion);
    bool isAPiece(Case casePion);
    bool verifDeplacement(CaseWithColor originCase, CaseWithColor destinationCase);
    void pieceMustBeChangeToDame();
    void saisieMouvementObligatoire(int& currentPlayer, std::vector<Case> casesObligatoires);
    void saisieMouvements(int& currentPlayer);
    bool verifDeplacementDame(CaseWithColor originCase, CaseWithColor destinationCase);
    bool verifDeplacementPion(CaseWithColor originCase, CaseWithColor destinationCase);
    bool findElement(std::vector<Case> casesObligatoires, Case saisie);
    void jeuGestionPion(int& currentPlayer);
    Piece* pieceOnCase(int x, int y);
    Piece* pieceOnCase(CaseWithColor caseOrigin);
    std::vector<Case> caseAfterPrisesPion(CaseWithColor originCase);
    std::vector<Case> caseAfterPrisesDames(CaseWithColor originCase);
    Case saisieDestination(int& idPlayer) override;
    bool canMove(int& idPlayer);
    bool AdversaireCanMove(int& idPlayer);
    // Override Grille

    void print() override;
    void start() override;
    void reglement() override;
    bool gameWinner(int& idJoueur) override;
    Case saisiePlayer(int& idJoueur) override;
};

#endif /* GrilleDames_hpp */
