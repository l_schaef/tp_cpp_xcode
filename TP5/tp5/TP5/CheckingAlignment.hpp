//
//  CheckingAlignment.hpp
//  TP5
//
//  Created by Laura Schaefer on 15/11/2020.
//

#ifndef CheckingAlignment_hpp
#define CheckingAlignment_hpp

#include <stdio.h>
#include <vector>

class CheckingAlignment {
    public:
        virtual bool grilleFull() = 0;
        virtual bool completeLine(int idLine, int& idPlayer) = 0;
        virtual bool completeRow(int idRow, int& idPlayer) = 0;
        virtual bool completeDiagonal(int idDiagonal, int& idPlayer) = 0;
};

#endif /* CheckingGrille_hpp */

