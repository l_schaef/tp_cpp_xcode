//
//  Piece.hpp
//  TP4
//
//  Created by Laura Schaefer on 15/11/2020.
//

#ifndef Piece_hpp
#define Piece_hpp

#include <stdio.h>
#include <iostream>
#include "EnumPion.hpp"

class Piece {
    
public :
    Piece(ECouleurPion pieceColor);
    ECouleurPion PieceColor;
    ETypePion Type;
    void changePionToDame();
    
    ~Piece();
};



#endif /* Pion_hpp */
