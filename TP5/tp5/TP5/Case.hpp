//
//  Case.hpp
//  CPP
//
//  Created by Thomas Durst on 14/10/2020.
//  Copyright © 2020 Thomas Durst. All rights reserved.
//

#ifndef Case_hpp
#define Case_hpp

#include <stdio.h>

class Case {
    public:
        Case(int _X, int _Y);
        int getX();
        int getY();
    private:
        int m_X;
        int m_Y;
};

#endif /* Case_hpp */
