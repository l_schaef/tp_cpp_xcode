//
//  Pion.cpp
//  TP4
//
//  Created by Laura Schaefer on 15/11/2020.
//

#include "Piece.hpp"

Piece::Piece(ECouleurPion pieceColor){
    PieceColor = pieceColor;
    Type = ETypePion::Pion;
}

Piece::~Piece(){
    std::cout << "Pion mangé !" << std::endl;
}

void Piece::changePionToDame(){
    Type = ETypePion::Dame;
}
