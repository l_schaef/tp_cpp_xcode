//
//  GrilleCouleur.cpp
//  tp5
//
//  Created by Thomas Durst on 17/11/2020.
//  Represente les grille à deux couleurs de case

#include "GrilleCouleur.hpp"

void GrilleCouleur::reset(){
    for (int i = 0; i < m_Grille.size(); i++) {
        for (int j = 0; j < m_Grille[i].size(); j++) {
            // Code
            if(m_Grille[i][j].pieceOnCase != nullptr){
                delete m_Grille[i][j].pieceOnCase;
                m_Grille[i][j].pieceOnCase = nullptr;
            }
        }
    }
}
