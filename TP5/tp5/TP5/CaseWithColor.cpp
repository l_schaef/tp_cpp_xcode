//
//  ColorCase.cpp
//  TP5
//
//  Created by Laura Schaefer on 15/11/2020.
//

#include "CaseWithColor.hpp"

CaseWithColor::CaseWithColor(int _X, int _Y, ECouleurCase couleurCase) : Case(_X, _Y), Color(couleurCase)
{
    pieceOnCase = nullptr;
}
