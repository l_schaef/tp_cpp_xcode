//
//  CaseWithValue.hpp
//  TP4
//
//  Created by Laura Schaefer on 15/11/2020.
//

#ifndef CaseWithValue_hpp
#define CaseWithValue_hpp

#include <stdio.h>
#include "Case.hpp"

#define VIDE 0

class CaseWithValue: public Case {
    
public:
    CaseWithValue(int _X, int _Y);
    int Val;
};

#endif /* CaseWithValue_hpp */
