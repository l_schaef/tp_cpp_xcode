//
//  Grille.cpp
//  CPP
//
//  Created by Thomas Durst on 20/10/2020.
//  Copyright © 2020 Thomas Durst. All rights reserved.
//

#include "Grille.hpp"

void Grille::nextPlayer(int& PlayerCourant){
    if(PlayerCourant == 0){ //Initialisation des joueurs à 0
        PlayerCourant = PLAYER_1;
    } else if(PlayerCourant == PLAYER_1){
        PlayerCourant = PLAYER_2;
    } else if(PlayerCourant == PLAYER_2) {
        PlayerCourant = PLAYER_1;
    }
}
