#include "GrilleDames.hpp"
#include <algorithm>
#include <iostream>

GrilleDames::GrilleDames(){
    bool isWhite = true;
    bool beginWithWhite = true;
    for (int i = 0; i < NB_LINE; i++) {
        if(beginWithWhite){
            isWhite = true;
        } else {
            isWhite = false;
        }
        std::vector<CaseWithColor> tmp;
        for (int j = 0; j < NB_ROW; j++) {
            if(isWhite){
                tmp.push_back(CaseWithColor(i, j, ECouleurCase::Blanche));
                isWhite = false;
            } else {
                tmp.push_back(CaseWithColor(i, j, ECouleurCase::Noire));
                isWhite = true;
            }
        }
        m_Grille.push_back(tmp);
        if(beginWithWhite){
            beginWithWhite = false;
        } else {
            beginWithWhite = true;
        }
    }
}

void GrilleDames::initialisePieces(){
    for (int i = 0; i < NB_LINE; i++) {
        for (int j = 0; j < NB_ROW; j++) {
            if(i < 4){
                if(m_Grille[i][j].Color == ECouleurCase::Noire){
                    m_Grille[i][j].pieceOnCase = new Piece(ECouleurPion::Noir);
                }
            } else if(i > 5){
                if(m_Grille[i][j].Color == ECouleurCase::Noire){
                    m_Grille[i][j].pieceOnCase = new Piece(ECouleurPion::Blanc);
                }
            }
        }
    }
}

// Override Grille

void GrilleDames::reglement(){
    std::cout << "          Bienvenue dans le jeu des Dames !" << std::endl;
    std::cout << std::endl;
    std::cout << "Voici les règles : " << std::endl;
    std::cout << "Le jeu se pratique sur un damier de 10 cases sur 10, orienté avec une case foncée en bas à gauche. Chaque joueur possède 20 pions, placés sur les cases foncées des 4 premières rangées." << std::endl;
    std::cout << "Les joueurs jouent chacun à leur tour. Les blancs commencent toujours." << std::endl;
    std::cout << "Le but du jeu est de capturer tous les pions adverses. " << std::endl;
    std::cout << "Si un joueur ne peut plus bouger, même s'il lui reste des pions, il perd la partie." << std::endl;
    std::cout << "Chaque pion peut se déplacer d'une case vers l'avant en diagonale." << std::endl;
    std::cout << "Un pion arrivant sur la dernière rangée et s'y arrêtant est promu en « dame ». " << std::endl;
    std::cout << "La dame se déplace sur une même diagonale d'autant de cases qu'elle le désire, en avant et en arrière." << std::endl;
    std::cout << std::endl;
    std::cout << "La prise par un pion : " << std::endl;
    std::cout << "Un pion peut en prendre un autre en sautant par dessus le pion adverse pour se rendre sur la case vide située derrière celui-ci. Le pion sauté est retiré du jeu. " << std::endl;
    std::cout << "La prise peut également s'effectuer en arrière. " << std::endl;
    std::cout << std::endl;
    std::cout << "La prise est obligatoire." << std::endl;
    std::cout << std::endl;
    std::cout << "Si, après avoir pris un premier pion, vous vous retrouvez de nouveau en position de prise, vous devez continuer, jusqu'à ce que cela ne soit plus possible. " << std::endl;
    std::cout << "Les pions doivent être enlevés à la fin de la prise et non pas un par un au fur et à mesure." << std::endl;
    std::cout << std::endl;
    std::cout << "La prise majoritaire : " << std::endl;
    std::cout << "Lorsque plusieurs prises sont possibles, il faut toujours prendre du côté du plus grand nombre de pièces." << std::endl;
    std::cout << "Cela signifie que si on peut prendre une dame ou deux pions, il faut prendre les deux pions" << std::endl;
    std::cout << "La prise par la dame : " << std::endl;
    std::cout << "Puisque la dame a une plus grande marge de manoeuvre, elle a aussi de plus grandes possibilités pour les prises." << std::endl;
    std::cout << "La dame doit prendre tout pion situé sur sa diagonale (s'il y a une case libre derrière) et doit changer de direction à chaque fois qu'une  nouvelle prise est possible. " << std::endl;
    std::cout << "On ne peut passer qu'une seule fois sur un même pion. En revanche, on peut passer deux fois sur la même case. " << std::endl;
    std::cout << "Enfin, la partie peut être déclarée nulle si aucun des deux joueurs ne peut prendre toutes les pièces adverses (par exemple 3 dames contre une)." << std::endl;
    std::cout << "Bon jeu !" << std::endl;
    std::cout << "Saisir les pions à déplacer : " << std::endl;
    std::cout << "Pion à déplacer : 4 5" << std::endl;
    std::cout << "Déplacé en : 5 6" << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
}

void GrilleDames::start(){
    reglement();
    initialisePieces();
    int currentPlayer = 0;
    do{
        nextPlayer(currentPlayer);
        print();
        jeuGestionPion(currentPlayer);
        std::cout << std::endl;
    } while(!(gameWinner(currentPlayer)));

    if(gameWinner(currentPlayer)) {
        std::cout << "Bravo joueur " << currentPlayer << " tu as gagné" << std::endl;
    }
    std::cout << "Au revoir ! A bientôt !" << std::endl;
}

Case GrilleDames::saisiePlayer(int& player){
    int x = 0, y = 0;
    std::cout << "Quel pion veux-tu déplacer ?" << std::endl;
    while(!(std::cin >> x >> y) || x < 1 || x > 11 || y < 1 || y > 11 || !(isAPiece(Case(x-1, y-1))) || couleurPionCase(Case(x-1, y-1)) != couleurJoueur(player)){
        if(std::cin.fail()){
            std::cout << "Erreur de saisie. Re-saisir : " << std::endl;
            std::cin.clear();
            std::cin.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
        } else if (x < 1 || x > 11 || y < 1 || y > 11) {
            std::cout << "Erreur de saisie. Re-saisir entre 1 et 10 : " << std::endl;
        } else if (!(isAPiece(Case(x-1, y-1)))) {
            std::cout << "Erreur pas de pion sur cette case. Re-saisir une case : " << std::endl;
        } else if (couleurPionCase(Case(x-1, y-1)) != couleurJoueur(player)) {
            std::cout << "Pas de pion à vous sur cette case. Re-saisir une nouvelle case : " << std::endl;
        }
    }
    return Case(x-1, y-1);
}

void GrilleDames::print(){
    std::cout << "     1   2   3   4   5   6   7   8   9   10 " << std::endl;
    std::cout << "   ╔═══╦═══╦═══╦═══╦═══╦═══╦═══╦═══╦═══╦═══╗" << std::endl;
    for (int i = 0; i < m_Grille.size(); i++) {
        if(i == 9){
            std::cout << i+1 << " ";
        } else {
            std::cout << i+1 << "  ";
        }
        for (int j = 0; j < m_Grille[i].size(); j++) {
            std::cout << "║ ";
            if(m_Grille[i][j].Color == ECouleurCase::Blanche){
                std::cout << "▢ ";
            } else if(pieceOnCase(i, j) == nullptr){
                std::cout << "  ";
            } else {
                std::cout << showTypePiece(pieceOnCase(i, j));
                
                std::cout << " ";
            }
        }
        std::cout << "║ " << i+1 << std::endl;
        if(i != m_Grille.size() - 1){
            std::cout << "   ╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣"<< std::endl;
        }
    }
    std::cout << "   ╚═══╩═══╩═══╩═══╩═══╩═══╩═══╩═══╩═══╩═══╝" << std::endl;
    std::cout << "     1   2   3   4   5   6   7   8   9   10 " << std::endl;
}

bool GrilleDames::gameWinner(int& idJoueur){
    int compteurPion = 0;
    if (!AdversaireCanMove(idJoueur)) {
        return true;
    }
    if(couleurJoueur(idJoueur) == ECouleurPion::Blanc){
        for (int i = 0; i < NB_LINE; i++) {
            for (int j = 0; j < NB_ROW; j++) {
                if(pieceOnCase(i, j) != nullptr && pieceOnCase(i, j)->PieceColor == ECouleurPion::Blanc){
                    compteurPion++;
                }
            }
        }
    } else {
        for (int i = 0; i < NB_LINE; i++) {
            for (int j = 0; j < NB_ROW; j++) {
                if(pieceOnCase(i, j) != nullptr && pieceOnCase(i, j)->PieceColor == ECouleurPion::Noir){
                    compteurPion++;
                }
            }
        }
    }
    return (compteurPion == 0);
}

void GrilleDames::moveAPion(Case originCase, Case caseToPlacePion){
    if(pieceOnCase(caseToPlacePion.getX(), caseToPlacePion.getY()) == nullptr){
        m_Grille[caseToPlacePion.getX()][caseToPlacePion.getY()].pieceOnCase = m_Grille[originCase.getX()][originCase.getY()].pieceOnCase;
        m_Grille[originCase.getX()][originCase.getY()].pieceOnCase = nullptr;
    }
}

char GrilleDames::showTypePiece(Piece *piece){
    char valueOfPiece = NULL;
    if(piece->Type == ETypePion::Pion && piece->PieceColor == ECouleurPion::Noir){
        valueOfPiece = 'x';
    } else if(piece->Type == ETypePion::Dame && piece->PieceColor == ECouleurPion::Noir) {
        valueOfPiece = 'X';
    } else if(piece->Type == ETypePion::Pion && piece->PieceColor == ECouleurPion::Blanc) {
        valueOfPiece = 'o';
    } else if(piece->Type == ETypePion::Dame && piece->PieceColor == ECouleurPion::Blanc) {
        valueOfPiece = 'O';
    }
    return valueOfPiece;
}

void GrilleDames::takePiece(Case pionTaken){
    delete m_Grille[pionTaken.getX()][pionTaken.getY()].pieceOnCase;
    m_Grille[pionTaken.getX()][pionTaken.getY()].pieceOnCase = nullptr;
}
/*
  [] -> Case libre
 o
x
Revoie la liste des cases libres apres prises
 */
std::vector<Case> GrilleDames::caseAfterPrises(CaseWithColor originCase){
    std::vector<Case> listOfCase;
    if(pieceOnCase(originCase) != nullptr){
        if(pieceOnCase(originCase)->Type == ETypePion::Pion){
            listOfCase = caseAfterPrisesPion(originCase);
        } else if(pieceOnCase(originCase)->Type == ETypePion::Dame){
            listOfCase = caseAfterPrisesDames(originCase);
        }
    }
    return listOfCase;
}

int GrilleDames::maxPrisesOfPiece(CaseWithColor caseOrigin, ECouleurPion color){
    int prise = 0;
    int compteur = 0;
    int nb = size(caseAfterPrises(caseOrigin));
    if(nb != 0){
        prise++;
        std::vector<Case> listDesCasesPossible = caseAfterPrises(caseOrigin);
        for (int i = 0; i < nb; i++) {
            if(size(caseAfterPrises(m_Grille[listDesCasesPossible[i].getX()][listDesCasesPossible[i].getY()])) > 0){
                compteur++;
            }
            compteur += maxPrisesOfPiece(m_Grille[listDesCasesPossible[i].getX()][listDesCasesPossible[i].getY()], color);
        }
    }
    return (prise + compteur);
}

bool GrilleDames::coup_obligatoire(ECouleurPion color){
    int nbcoup = 0;
    for (int i = 0; i < m_Grille.size(); i++) {
        for(int j = 0; j < m_Grille[i].size(); j++){
            if(pieceOnCase(i, j) != nullptr && pieceOnCase(i, j)->PieceColor == color && nbcoup <= maxPrisesOfPiece(m_Grille[i][j], color)){
                nbcoup = maxPrisesOfPiece(m_Grille[i][j], color);
            }
        }
    }
    return (nbcoup != 0);
}

int GrilleDames::size(std::vector<Case> list){
    int cpt = 0;
    for (int i = 0; i < list.size(); i++) {
        cpt++;
    }
    return cpt;
}

std::vector<Case> GrilleDames::caseObligatoire(ECouleurPion color){
    int nbcoup = 0;
    std::vector<Case> lc;
    for (int i = 0; i < m_Grille.size(); i++) {
        for(int j = 0; j < m_Grille[i].size(); j++){
            if(pieceOnCase(i, j) != nullptr && pieceOnCase(i, j)->PieceColor == color && nbcoup < maxPrisesOfPiece(m_Grille[i][j], color)){
                lc.push_back(Case(i, j));
                nbcoup = maxPrisesOfPiece(m_Grille[i][j], color);
            } else if(pieceOnCase(i, j) != nullptr && nbcoup == maxPrisesOfPiece(m_Grille[i][j], color) && nbcoup != 0 && pieceOnCase(i, j)->PieceColor == color) {
                lc.push_back(Case(i, j));
                nbcoup = maxPrisesOfPiece(m_Grille[i][j], color);
            }
        }
    }
    return lc;
}

ECouleurPion GrilleDames::couleurJoueur(int& IdJoueur){
    if(IdJoueur == 1){
        return ECouleurPion::Blanc;
    } else {
        return ECouleurPion::Noir;
    }
}

ECouleurPion GrilleDames::couleurPionCase(Case casePion){
    return pieceOnCase(casePion.getX(), casePion.getY())->PieceColor;
}

bool GrilleDames::isAPiece(Case casePion){
    if(pieceOnCase(casePion.getX(), casePion.getY()) != nullptr){
        return true;
    } else {
        return false;
    }
}

Case GrilleDames::saisieDestination(int& idPlayer){
    int x = 0, y = 0;
    std::cout << "Où veux-tu le déplacer ?" << std::endl;
    while(!(std::cin >> x >> y) || x < 1 || x > 11 || y < 1 || y > 11 || pieceOnCase(x-1, y-1) != nullptr || m_Grille[x-1][y-1].Color == ECouleurCase::Blanche){
        if(std::cin.fail()){
            std::cout << "Erreur de saisie. Re-saisir : " << std::endl;
            std::cin.clear();
            std::cin.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
        } else if (x < 1 || x > 11 || y < 1 || y > 11) {
            std::cout << "Erreur de saisie. Re-saisir entre 1 et 10 : " << std::endl;
        } else if (pieceOnCase(x-1, y-1) != nullptr) {
            std::cout << "Erreur il y a déjà un pion sur cette case. Re-saisir une case : " << std::endl;
        } else if (m_Grille[x-1][y-1].Color == ECouleurCase::Blanche) {
            std::cout << "Erreur on ne peut que aller sur les cases noires. Re-saisir une case : " << std::endl;
        }
    }
    return Case(x-1, y-1);
}

bool GrilleDames::verifDeplacement(CaseWithColor originCase, CaseWithColor destinationCase){
    if(pieceOnCase(originCase)->Type == ETypePion::Pion){
        return verifDeplacementPion(originCase, destinationCase);
    } else {
        
    }
    return false;
}

void GrilleDames::pieceMustBeChangeToDame(){
    for(int i = 0; i < NB_ROW; i++){
        if(pieceOnCase(0, i) != nullptr && pieceOnCase(0, i)->PieceColor == ECouleurPion::Blanc && pieceOnCase(0, i)->Type != ETypePion::Dame){
            pieceOnCase(0, i)->changePionToDame();
        }
        if(pieceOnCase(9, i) != nullptr && pieceOnCase(9, i)->PieceColor == ECouleurPion::Noir && pieceOnCase(9, i)->Type != ETypePion::Dame){
            pieceOnCase(9, i)->changePionToDame();
        }
    }
}

void GrilleDames::jeuGestionPion(int& currentPlayer){
    std::cout << std::endl;
    std::cout << "C'est à toi joueur " << currentPlayer << " : " << std::endl;
    if(!(coup_obligatoire(couleurJoueur(currentPlayer)))){
        saisieMouvements(currentPlayer);
    } else {
        while (coup_obligatoire(couleurJoueur(currentPlayer))) {
            std::cout << "Vous avez un coup obligatoire ! " << std::endl;
            int n = size(caseObligatoire(couleurJoueur(currentPlayer)));
            for (int i = 0; i < n; i++) {
                std::cout << "X : " << caseObligatoire(couleurJoueur(currentPlayer))[i].getX() + 1 << " Y : "<< caseObligatoire(couleurJoueur(currentPlayer))[i].getY() + 1 << std::endl;
            }
            saisieMouvements(currentPlayer);
            print();
        }
    }
    pieceMustBeChangeToDame();
}

bool GrilleDames::verifDeplacementDame(CaseWithColor originCase, CaseWithColor destinationCase){
    int tmpX = destinationCase.getX() - originCase.getX();
    int tmpY = destinationCase.getY() - originCase.getY();

    if(std::abs(tmpX) == std::abs(tmpY)){
        int compteur = 0;
        int x = originCase.getX();
        int y = originCase.getY();
        while (x != destinationCase.getX()) {
            if(tmpX < 0 && tmpY > 0){
                x -= 1;
                y += 1;
            } else if(tmpX < 0 && tmpY < 0){
                x -= 1;
                y -= 1;
            } else if(tmpX > 0 && tmpY > 0){
                x += 1;
                y += 1;
            } else if(tmpX > 0 && tmpY < 0){
                x += 1;
                y -= 1;
            }
            if(pieceOnCase(x, y)->PieceColor == pieceOnCase(originCase)->PieceColor){
                return false;
            }
            if(pieceOnCase(x, y)->PieceColor != pieceOnCase(originCase)->PieceColor){
                compteur++;
            } else {
                compteur = 0;
            }
            if(compteur > 1){
                return false;
            }
        }
    }
    return true;
}

bool GrilleDames::verifDeplacementPion(CaseWithColor originCase, CaseWithColor destinationCase){
    int tmpX = destinationCase.getX() - originCase.getX();
    int tmpY = destinationCase.getY() - originCase.getY();
    
    if(((tmpX == -1 && tmpY == -1 && couleurPionCase(originCase) == ECouleurPion::Blanc) || (tmpX == -1 && tmpY == 1 && couleurPionCase(originCase) == ECouleurPion::Blanc) || (tmpX == 1 && tmpY == -1 && couleurPionCase(originCase) == ECouleurPion::Noir)|| (tmpX == 1 && tmpY == 1 && couleurPionCase(originCase) == ECouleurPion::Noir)) && pieceOnCase(destinationCase) == nullptr){
        return true;
    } else if (std::abs(tmpX) == 2 && std::abs(tmpY) == 2 && pieceOnCase(destinationCase) == nullptr){
        int tmpX2 = (destinationCase.getX() + originCase.getX()) / 2;
        int tmpY2 = (destinationCase.getY() + originCase.getY()) / 2;
        if(couleurPionCase(originCase) == ECouleurPion::Blanc && couleurPionCase(m_Grille[tmpX2][tmpY2]) == ECouleurPion::Noir){
            return true;
        } else if(couleurPionCase(originCase) == ECouleurPion::Noir && couleurPionCase(m_Grille[tmpX2][tmpY2]) == ECouleurPion::Blanc){
            return true;
        }
    }
    return false;
}

void GrilleDames::saisieMouvements(int& currentPlayer){
    Case tmpOrigin = saisiePlayer(currentPlayer);
    Case tmpDestination = saisieDestination(currentPlayer);
    while(!(verifDeplacement(m_Grille[tmpOrigin.getX()][tmpOrigin.getY()], m_Grille[tmpDestination.getX()][tmpDestination.getY()]))){
        std::cout << "Vous ne pouvez pas déplacer ce pion vers cette case !" << std::endl;
        std::cout << "Choisir un autre pion." << std::endl;
        tmpOrigin = saisiePlayer(currentPlayer);
        tmpDestination = saisieDestination(currentPlayer);
        
    }
    moveAPion(tmpOrigin, tmpDestination);
    if(std::abs(tmpOrigin.getX() - tmpDestination.getX()) == 2){
        takePiece(Case((tmpOrigin.getX() + tmpDestination.getX())/2, (tmpOrigin.getY() + tmpDestination.getY())/2));
    }
}

void GrilleDames::saisieMouvementObligatoire(int& currentPlayer, std::vector<Case> casesObligatoires){
    Case tmpOrigin = saisiePlayer(currentPlayer);
    while(!(findElement(casesObligatoires, tmpOrigin))){
        tmpOrigin = saisiePlayer(currentPlayer);
    }
    Case tmpDestination = saisieDestination(currentPlayer);
    while(!(verifDeplacement(m_Grille[tmpOrigin.getX()][tmpOrigin.getY()], m_Grille[tmpDestination.getX()][tmpDestination.getY()]))){
        std::cout << "Vous ne pouvez pas déplacer ce pion vers cette case !" << std::endl;
        std::cout << "Choisir un autre pion." << std::endl;
        tmpOrigin = saisiePlayer(currentPlayer);
        tmpDestination = saisieDestination(currentPlayer);
        
    }
    moveAPion(tmpOrigin, tmpDestination);
    if(std::abs(tmpOrigin.getX() - tmpDestination.getX()) == 2){
        takePiece(Case((tmpOrigin.getX() + tmpDestination.getX())/2, (tmpOrigin.getY() + tmpDestination.getY())/2));
    }
}

bool GrilleDames::findElement(std::vector<Case> casesObligatoires, Case saisie){
    for (int i = 0; i < size(casesObligatoires); i++) {
        if(casesObligatoires[i].getX() == saisie.getX() && casesObligatoires[i].getY() == saisie.getY()){
            return true;
        }
    }
    return false;
}

Piece* GrilleDames::pieceOnCase(int x, int y){
    return m_Grille[x][y].pieceOnCase;
}

Piece* GrilleDames::pieceOnCase(CaseWithColor caseOrigin){
    return caseOrigin.pieceOnCase;
}

std::vector<Case> GrilleDames::caseAfterPrisesPion(CaseWithColor originCase){
    std::vector<Case> listOfCase;
    if(originCase.getX() < NB_LINE - 2 && originCase.getY() > 1 && pieceOnCase(originCase.getX() + 1, originCase.getY() - 1) != nullptr && pieceOnCase(originCase.getX() + 1, originCase.getY() - 1)->PieceColor != pieceOnCase(originCase)->PieceColor){
        if(pieceOnCase(originCase.getX() + 2, originCase.getY() - 2) == nullptr){
            // prise dispo
            listOfCase.push_back(Case(originCase.getX() + 2, originCase.getY() - 2));
        }
    }
    if(originCase.getX() < NB_LINE - 2 && originCase.getY() < NB_ROW - 2 && pieceOnCase(originCase.getX() + 1, originCase.getY() + 1) != nullptr && pieceOnCase(originCase.getX() + 1, originCase.getY() + 1)->PieceColor != pieceOnCase(originCase)->PieceColor){
        if(pieceOnCase(originCase.getX() + 2, originCase.getY() + 2) == nullptr){
            listOfCase.push_back(Case(originCase.getX() + 2, originCase.getY() + 2));
        }
    }
    if(originCase.getY() > 1 && originCase.getX() > 1 && pieceOnCase(originCase.getX() - 1, originCase.getY() - 1) != nullptr && pieceOnCase(originCase.getX() - 1, originCase.getY() - 1)->PieceColor != pieceOnCase(originCase)->PieceColor){
        if(pieceOnCase(originCase.getX() - 2, originCase.getY() - 2) == nullptr){
            listOfCase.push_back(Case(originCase.getX() - 2, originCase.getY() - 2));
        }
    }
    if(originCase.getX() > 1 && originCase.getY() < NB_ROW - 2 && pieceOnCase(originCase.getX() - 1, originCase.getY() + 1) != nullptr && pieceOnCase(originCase.getX() - 1, originCase.getY() + 1)->PieceColor != pieceOnCase(originCase)->PieceColor){
        if(pieceOnCase(originCase.getX() - 2, originCase.getY() + 2) == nullptr){
            listOfCase.push_back(Case(originCase.getX() - 2, originCase.getY() + 2));
        }
    }
    return listOfCase;
}

std::vector<Case> GrilleDames::caseAfterPrisesDames(CaseWithColor originCase){
    std::vector<Case> listOfCase;
    int tmpX = originCase.getX();
    int tmpY = originCase.getY();
    
    if(originCase.getX() < NB_LINE - 2 && originCase.getY() > 1){
        while(tmpX < NB_LINE-1 && tmpY > 1){
            tmpX += 1;
            tmpY -= 1;
            if(pieceOnCase(tmpX, tmpY) != nullptr){
                if(pieceOnCase(tmpX, tmpY)->PieceColor == pieceOnCase(originCase)->PieceColor || pieceOnCase(tmpX, tmpY)->Type == ETypePion::Dame){
                    break;
                } else if(pieceOnCase(tmpX+1, tmpY-1) == nullptr){
                    listOfCase.push_back(Case(tmpX+1, tmpY-1));
                }
            }
        }
    }
    if(originCase.getX() < NB_LINE - 2 && originCase.getY() < NB_ROW - 2){
        while(tmpX < NB_LINE-1 && tmpY < NB_ROW-1){
            tmpX += 1;
            tmpY += 1;
            if(pieceOnCase(tmpX, tmpY) != nullptr){
                if(pieceOnCase(tmpX, tmpY)->PieceColor == pieceOnCase(originCase)->PieceColor || pieceOnCase(tmpX, tmpY)->Type == ETypePion::Dame){
                    break;
                } else if(pieceOnCase(tmpX+1, tmpY+1) == nullptr){
                    listOfCase.push_back(Case(tmpX+1, tmpY+1));
                }
            }
        }
    }
    if(originCase.getY() > 1 && originCase.getX() > 1){
        while(tmpX > 1 && tmpY > 1){
            tmpX -= 1;
            tmpY -= 1;
            if(pieceOnCase(tmpX, tmpY) != nullptr){
                if(pieceOnCase(tmpX, tmpY)->PieceColor == pieceOnCase(originCase)->PieceColor || pieceOnCase(tmpX, tmpY)->Type == ETypePion::Dame){
                    break;
                } else if(pieceOnCase(tmpX-1, tmpY-1) == nullptr){
                    listOfCase.push_back(Case(tmpX-1, tmpY-1));
                }
            }
        }
    }
    if(originCase.getX() > 1 && originCase.getY() < NB_ROW - 2){
        while(tmpX > 1 && tmpY < NB_ROW-1){
            tmpX -= 1;
            tmpY += 1;
            if(pieceOnCase(tmpX, tmpY) != nullptr){
                if(pieceOnCase(tmpX, tmpY)->PieceColor == pieceOnCase(originCase)->PieceColor || pieceOnCase(tmpX, tmpY)->Type == ETypePion::Dame){
                    break;
                } else if(pieceOnCase(tmpX-1, tmpY+1) == nullptr){
                    listOfCase.push_back(Case(tmpX-1, tmpY-1));
                }
            }
        }
    }
    return listOfCase;
}

bool GrilleDames::AdversaireCanMove(int& idPlayer){
    for (int i = 0; i < m_Grille.size(); i++) {
        for (int j = 0; j < m_Grille[i].size(); j++) {
            if (pieceOnCase(i, j)->PieceColor == (idPlayer == 2 ? ECouleurPion::Blanc : ECouleurPion::Noir)) {

                if(m_Grille[i][j].getX() > 0 && m_Grille[i][j].getY() > 0 && pieceOnCase(i-1, j-1)){
                        return true;
                }
                if(m_Grille[i][j].getX() > 1 && m_Grille[i][j].getY() > 1 && pieceOnCase(i-2, j-2)){
                        return true;
                }
                if(m_Grille[i][j].getX() > 0 && m_Grille[i][j].getY() < NB_ROW && pieceOnCase(i-1, j+1)){
                        return true;
                }
                if(m_Grille[i][j].getX() > 1 && m_Grille[i][j].getY() < NB_ROW-1 && pieceOnCase(i-2, j+2)){
                        return true;
                }
                if(m_Grille[i][j].getX() < NB_LINE && m_Grille[i][j].getY() < NB_ROW && pieceOnCase(i+1, j+1)){
                        return true;
                }
                if(m_Grille[i][j].getX() < NB_LINE-1 && m_Grille[i][j].getY() < NB_ROW-1 && pieceOnCase(i+2, j+2)){
                        return true;
                }
                if(m_Grille[i][j].getX() < NB_LINE && m_Grille[i][j].getY() > 0 && pieceOnCase(i+1, j-1)){
                        return true;
                }
                if(m_Grille[i][j].getX() < NB_LINE-1 && m_Grille[i][j].getY() > 1 && pieceOnCase(i+2, j-2)){
                        return true;
                }
            }
        }
    }
    return false;
}

