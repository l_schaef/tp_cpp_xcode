//
//  Grille.hpp
//  CPP
//
//  Created by Thomas Durst on 20/10/2020.
//  Copyright © 2020 Thomas Durst. All rights reserved.
//

#ifndef Grille_hpp
#define Grille_hpp

#include <stdio.h>
#include "Case.hpp"
#include <iostream>

#define PLAYER_1 1
#define PLAYER_2 2
#define VIDE 0

class Grille {
    public:
        void nextPlayer(int& playerCourant);
        
        virtual void reset() = 0;
        virtual void print() = 0;
        virtual void start() = 0;
        virtual void reglement() = 0;
        virtual bool gameWinner(int& idJoueur) = 0;
        virtual Case saisiePlayer(int& idJoueur) = 0;
};
#endif /* Grille_hpp */
