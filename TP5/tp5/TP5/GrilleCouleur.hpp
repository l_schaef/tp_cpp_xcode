//
//  GrilleCouleur.hpp
//  tp5
//
//  Created by Thomas Durst on 17/11/2020.
//

#ifndef GrilleCouleur_hpp
#define GrilleCouleur_hpp

#include <stdio.h>
#include <vector>
#include "Grille.hpp"
#include "Case.hpp"
#include "CaseWithColor.hpp"

class GrilleCouleur : public Grille {
    public:
        void reset() override;
        virtual Case saisieDestination(int& idPlayer) = 0;
    protected:
        std::vector<std::vector<CaseWithColor>> m_Grille;
};

#endif /* GrilleCouleur_hpp */
