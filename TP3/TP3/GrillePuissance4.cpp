//
//  GrillePuissance4.cpp
//  TP3
//
//  Created by Laura Schaefer on 27/10/2020.
//
#include "Case.hpp"
#include <iostream>
#include <stdbool.h>
#include "GrillePuissance4.hpp"

GrillePuissance4::GrillePuissance4(){
    largeur = 7;
    hauteur = 4;
    for(int i = 1; i <= hauteur; i++){
        std::vector<Case> ligne;
        for(int j = 1; j <= largeur; j++){
            Case c{i,j};
            ligne.push_back(c);
        }
        grille.push_back(ligne);
    }
}


void GrillePuissance4::Print(){
    for(int i = 0; i < grille.size(); i++){
        for(int j = 0; j < grille[i].size(); j++){
            std::cout << grille[i][j].val << " ";
        }
        std::cout << std::endl;
    }
}

bool GrillePuissance4::ColonneRemplie(int colonne){
    return (grille[0][colonne-1].val != 0);
}

bool GrillePuissance4::CaseVide(Case c){
    return (grille[(c.x)-1][(c.y)-1].val == 0);
}

void GrillePuissance4::DeposerJeton(int colonne, int joueur){
    if (!ColonneRemplie(colonne)){
        for (int i = 4; i > 0; i--) {
            if (CaseVide(Case{i,colonne})){
                grille[i-1][colonne-1].val = joueur;
                break;
            }
        }
    }
}

bool GrillePuissance4::Ligne(int joueur)
{
    for (int i = 3; i >= 0; i--) {
        int compteur = 0;
        for (int j = 0; j < 7; j++){
            if(grille[i][j].val == joueur){
                compteur++;
                if (compteur == 4){
                    return true;
                }
            } else {
                compteur = 0;
            }
        }
    }
    return false;
}

bool GrillePuissance4::Colonne(int joueur){
    for (int j = 0; j < 7; j++){
        int compteur = 0;
        for (int i = 0; i < 4; i++) {
            if(grille[i][j].val == joueur){
                compteur++;
                if (compteur == 4){
                    return true;
                }
            } else {
                compteur = 0;
            }
        }
    }
    return false;
}

bool GrillePuissance4::Diagonale(int joueur){
    for (int x = 0; x < 4; x++) {
        int i = 3;
            int compteur = 0;
            for (int j = x; j < x+4; j++) {
                if(grille[i][j].val == joueur){
                    compteur++;
                    if (compteur == 4){
                        return true;
                    }
                } else {
                    compteur = 0;
                }
                i--;
            }
        }
    
    for (int x = 0; x < 4; x++) {
        int k = 0;
            int compteur = 0;
            for (int j = x; j < x+4; j++) {
                if(grille[k][j].val == joueur){
                    compteur++;
                    if (compteur == 4){
                        return true;
                    }
                } else {
                    compteur = 0;
                }
                k++;
            }
        }
    
    return false;
}

bool GrillePuissance4::VictoireJoueur(int joueur){
    return (Ligne(joueur) || Colonne(joueur) || Diagonale(joueur));
}

