#include "GrilleMorpion.hpp"
#include "Case.hpp"
#include <iostream>
#include <stdbool.h>

GrilleMorpion::GrilleMorpion()
{
    for(int i = 1; i <= 3; i++){
        std::vector<Case> ligne;
        for(int j = 1; j <= 3; j++){
            Case c{i,j};
            ligne.push_back(c);
        }
        grille.push_back(ligne);
    }
}

void GrilleMorpion::Print(){
    for(int i = 0; i < grille.size(); i++){
        for(int j = 0; j < grille[i].size(); j++){
            std::cout << grille[i][j].val << " ";
        }
        std::cout << std::endl;
    }
}

bool GrilleMorpion::CaseVide(Case c){
    return (grille[(c.x)-1][(c.y)-1].val == 0);
}

void GrilleMorpion::DeposerJeton(Case c, int joueur){
    grille[(c.x)-1][(c.y)-1].val = joueur;
}

bool GrilleMorpion::LigneComplete(int ligne, int joueur){
    for (int i = 0; i < 3; i++){
        if(!(grille[ligne-1][i].val == joueur))
            return false;
    }
    return true;
}

bool GrilleMorpion::ColonneComplete(int colonne, int joueur){
    for (int i = 0; i < 3; i++){
        if(!(grille[i][colonne-1].val == joueur))
            return false;
    }
    return true;
}

bool GrilleMorpion::DiagonaleComplete(int diagonale, int joueur){
    if (diagonale == 1){
        for (int i = 0; i < 3; i++){
            if (!(grille[i][i].val == joueur))
                return false;
        }
    } else if (diagonale == 2){
        int j = 2;
        for (int i = 0; i < 3; i++){
            if (!(grille[i][j].val == joueur))
                return false;
            j--;
        }
    }
    else {
        return false;
    }
    return true;
}

bool GrilleMorpion::VictoireJoueur(int joueur){
    bool victoire = false;
    for (int i = 1; i < 3; i++){
        if(DiagonaleComplete(i, joueur))
            victoire = true;
    }
    for (int j = 1; j < 4; j++){
        if(LigneComplete(j, joueur) || ColonneComplete(j, joueur))
            victoire = true;
    }
    return victoire;
}
