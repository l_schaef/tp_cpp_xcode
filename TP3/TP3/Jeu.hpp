#ifndef JEU_H
#define JEU_H
#include "GrilleMorpion.hpp"
#include "GrillePuissance4.hpp"


class Jeu
{
public:
    Jeu(GrilleMorpion gm);
    Jeu(GrillePuissance4 gp4);
    void Init(int x);
    void Reglement(int x);
    void Tour(int joueur, int x);
private:
    GrilleMorpion plateau;
    GrillePuissance4 cadrillage;
};

#endif // JEU_H
