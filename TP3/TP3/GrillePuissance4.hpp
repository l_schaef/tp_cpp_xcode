//
//  GrillePuissance4.hpp
//  TP3
//
//  Created by Laura Schaefer on 27/10/2020.
//

#ifndef GrillePuissance4_hpp
#define GrillePuissance4_hpp

#include <vector>
#include <stdio.h>
#include "Case.hpp"

class GrillePuissance4
{
public:
    GrillePuissance4();
    int largeur;
    int hauteur;
    void Print();
    bool CaseVide(Case c);
    bool ColonneRemplie(int colonne);
    void DeposerJeton(int colonne, int joueur);
    bool Ligne(int joueur);
    bool Colonne(int joueur);
    bool Diagonale(int joueur);
    bool VictoireJoueur(int joueur);
private:
    std::vector<std::vector<Case>> grille;
};

#endif /* GrillePuissance4_hpp */
