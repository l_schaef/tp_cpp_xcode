#ifndef GRILLEMORPION_H
#define GRILLEMORPION_H

#include <vector>
#include "Case.hpp"

class GrilleMorpion
{
public:
    GrilleMorpion();
    void Print();
    bool CaseVide(Case c);
    void DeposerJeton(Case c, int joueur);
    bool LigneComplete(int ligne, int joueur);
    bool ColonneComplete(int colonne, int joueur);
    bool DiagonaleComplete(int diagonale, int joueur);
    bool VictoireJoueur(int joueur);
private:
    std::vector<std::vector<Case>> grille;
};

#endif // GRILLEMORPION_H
