#ifndef CASE_H
#define CASE_H


class Case
{
public:
    Case();
    Case(int x, int y);
    int val;
    int x;
    int y;
};

#endif // CASE_H
