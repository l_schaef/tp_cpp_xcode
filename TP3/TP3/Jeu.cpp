#include "Jeu.hpp"
#include "Case.hpp"
#include "GrilleMorpion.hpp"
#include <iostream>

Jeu::Jeu(GrilleMorpion gm)
{
    plateau = gm;
    Init(1);
}

Jeu::Jeu(GrillePuissance4 gp4)
{
    cadrillage = gp4;
    Init(2);
}


void Jeu::Init(int jeu){
    Reglement(jeu);
    std::cout << "Que la partie commence !" << std::endl;
    int tour = 0;
    do {
        for(int j = 1; j <= 2; j++){
            Tour(j, jeu);
            tour++;
            if (plateau.VictoireJoueur(j) || cadrillage.VictoireJoueur(j)) {
                std::cout << "Victoire du joueur " << std::to_string(j) << " !" << std::endl;
                std::cout << "Fin de la partie. A bientôt !" << std::endl;
                break;
            }
        }
    } while ((jeu == 1 && tour < 9) || (jeu == 2 && tour < 28));
    std::string recommencer;
    std::cout << "Match nul, voulez-vous recommencer ? (OUI/NON)" << std::endl;
    do {
        std::cin >> recommencer;
        std::transform(recommencer.begin(), recommencer.end(), recommencer.begin(),
            [](unsigned char c){ return std::toupper(c); });
        if (!(recommencer == "OUI" || recommencer == "NON")) {
            std::cout << "Je n'ai pas bien compris. voulez-vous recommencer ? (OUI/NON)" << std::endl;
        }
    } while (!(recommencer == "OUI" || recommencer == "NON"));
    if (recommencer == "OUI"){
        Init(jeu);
    }
}

void Jeu::Reglement(int jeu){
    if (jeu == 1) {
        std::cout << "Une partie de morpion se joue sur une grille de 3 lignes et 3 colonnes. Le but du jeu est d'aligner avant son adversaire 3 symboles (1 ou 2) identiques horizontalement, verticalement ou en diagonale." << std::endl;
    } else {
        std::cout << "Le but du jeu du puissance 4 est d'aligner une suite de 4 pions (1 ou 2) sur une grille comptant 4 rangées et 7 colonnes. Tour à tour, les deux joueurs placent un pion dans la colonne de leur choix, le pion coulisse alors jusqu'à la position la plus basse possible dans la dite colonne à la suite de quoi c'est à l'adversaire de jouer. Le vainqueur est le joueur qui réalise le premier un alignement (horizontal, vertical ou diagonal) consécutif d'au moins quatre pions de sa couleur. Si, alors que toutes les cases de la grille de jeu sont remplies, aucun des deux joueurs n'a réalisé un tel alignement, la partie est déclarée nulle." << std::endl;
    }
}

void Jeu::Tour(int j, int jeu){
    if (jeu == 1) {
        Case c;
        int x = 0;
        int y = 0;
        std::cout << "Au tour du joueur " << std::to_string(j) << std::endl;
        do {
            std::cout << "Sur quelle case voulez-vous placer votre pion ?" << std::endl;
            std::cout << "Ligne : " << std::endl;
            while (!(std::cin >> x) || x > 3 || x < 1) {
                if (std::cin.fail()) {
                    std::cout << "Erreur de saisie. Ligne : " << std::endl;
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                }
                else if (x > 3 || x < 1) {
                    std::cout << "Votre entrée ne correspond pas à une ligne de la grille. Veuillez saisir un chiffre entre 1 et 3." << std::endl;
                }
            }
            std::cout << "Colonne : " << std::endl;
            while (!(std::cin >> y) || y > 3 || y < 1) {
                if (std::cin.fail()) {
                    std::cout << "Erreur de saisie. Colonne : " << std::endl;
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                }
                if (y > 3 || y < 1) {
                    std::cout << "Votre entrée ne correspond pas à une ligne de la grille. Veuillez saisir un chiffre entre 1 et 3." << std::endl;
                }
            }
            c.x = x;
            c.y = y;
            if(!plateau.CaseVide(c)){
                std::cout << "Un jeton est déjà posé sur cette case. Veuillez placer votre jeton sur une nouvelle case." << std::endl;
            }
        } while (!plateau.CaseVide(c));
        plateau.DeposerJeton(c, j);
        plateau.Print();
    } else {
        int y = 0;
        std::cout << "Au tour du joueur " << std::to_string(j) << std::endl;
        do {
            std::cout << "Dans quelle colonne voulez-vous placer votre pion ?" << std::endl;
            while (!(std::cin >> y) || y > 7 || y < 1) {
                if (std::cin.fail()) {
                    std::cout << "Erreur de saisie. Colonne : " << std::endl;
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                }
                if (y > 7 || y < 1) {
                    std::cout << "Votre entrée ne correspond pas à une ligne de la grille. Veuillez saisir un chiffre entre 1 et 7." << std::endl;
                }
            }
            if(cadrillage.ColonneRemplie(y)){
                std::cout << "Cette colonne est remplie. Veuillez placer votre jeton dans une nouvelle colonne." << std::endl;
            }
        } while (cadrillage.ColonneRemplie(y));
        cadrillage.DeposerJeton(y, j);
        cadrillage.Print();
    }
}
