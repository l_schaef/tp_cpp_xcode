//
//  main.cpp
//  TP1
//
//  Created by Laura Schaefer on 16/09/2020.
//  Copyright © 2020 Laura Schaefer. All rights reserved.
//

#include <iostream>
#include <time.h>
#include <cmath>

//I.1.1

int somme(int a, int b){
    return a + b;
}

//I.1.2

void inverse(int& a, int& b){
    int temp = a;
    a = b;
    b = temp;
}

//I.1.3-1

int sommePointeur(int a, int b, int* c){
    *c = a + b;
    return *c;
}

//I.1.3-2

int sommeReference(int a, int b, int& c){
    c = a + b;
    return c;
}

//I.1.4

void randomTab(int tab[], int taille){
    for (int i=0; i<taille; i++) {
        tab[i]= 1 + rand()%100;
    }
}

void miroir(int tab[], int tailleTableau){
    int i, j ;
    for(i = 0, j = tailleTableau-1 ; i < j ; i++ , j--){
        inverse(tab[i],tab[j]);
    }
}

void orderTab(int tab[], int tailleTableau, int sens) {
    int tour = 0;
    bool changer = true;
    while(changer){
        changer = false;
        tour++;
        if(sens == 1 || sens == 2){
            for (int i = 0; i < tailleTableau-tour; i++) {
                if ((sens == 1 && tab[i] > tab[i+1])||(sens == 2 && tab[i] < tab[i+1])) {
                    inverse(tab[i],tab[i+1]);
                    changer = true;
                }
            }
        }
    }
    std::cout << "Tableau trié : " << std::endl;
    for (int i = 0; i < tailleTableau; i++) {
        std::cout << tab[i] << std::endl;
    }
    std::cout << "----------------------------" << std::endl;
    
    miroir(tab, tailleTableau);
    
    std::cout << "Tableau inversé : " << std::endl;
    for (int i = 0; i < tailleTableau; i++) {
        std::cout << tab[i] << std::endl;
    }
}

//  II

std::string tennis(int ech1, int ech2){
    std::string s1;
    std::string s2;
    std::string result;
    if (ech1 <= 3 && ech2 <= 3) {
        switch (ech1) {
            case 0:
                s1 = "0";
                break;
            case 1:
                s1 = "15";
                break;
            case 2:
                s1 = "30";
                break;
            case 3:
                s1 = "40";
                break;
        }
        switch (ech2) {
            case 0:
                s2 = "0";
                break;
            case 1:
                s2 = "15";
                break;
            case 2:
                s2 = "30";
                break;
            case 3:
                s2 = "40";
                break;
        }
        if (ech1 == ech2)
            result = "le score est " + s1 + " - A";
        else
            result = "le score est " + s1 + " - " + s2 ;
    }
    else if (ech1 < 3 && ech2 > 3)
        result = "le jeu a été gagné par le joueur 2.";
    else if (ech2 < 3 && ech1 > 3)
        result = "le jeu a été gagné par le joueur 1.";
    else if (ech1 == ech2)
        result = "le score est à égalité." ;
    else if (ech1 - ech2 == 1)
        result = "le joueur 1 a l'avantage.";
    else if (ech2 - ech1 == 1)
        result = "le joueur 2 a l'avantage.";
    else if(ech1 - ech2 >= 2)
        result = "le jeu a été gagné par le joueur 1.";
    else if (ech2 - ech1 >= 2)
        result = "le jeu a été gagné par le joueur 1.";
    
    return result;
}

//  Tennis autre version

void match(){
    std::string scoreJ1 = "0";
    std::string scoreJ2 = "0";
    int joueur = 0 ;
    while (!(scoreJ1 == "GAGNE" || scoreJ2 == "GAGNE")) {
        while (!(joueur == 1 || joueur == 2)) {
            std::cout << "Quel joueur a gagné le point ? (Entrez 1 ou 2)" << std::endl;
            std::cin >> joueur;
        }
        if (joueur == 1){
            if (scoreJ1 == "0")
                scoreJ1 = "15";
            else if (scoreJ1 == "15")
                scoreJ1 = "30";
            else if (scoreJ1 == "30")
                scoreJ1 = "40";
            else if (scoreJ1 == "40"){
                if (scoreJ2 == "40")
                    scoreJ1 = "A";
                else if (scoreJ2 == "A")
                    scoreJ2 = "40";
                else
                    scoreJ1 = "GAGNE";
            }
            else if (scoreJ1 == "A")
                scoreJ1 = "GAGNE";
        }
        else if (joueur == 2){
            if (scoreJ2 == "0")
                scoreJ2 = "15";
            else if (scoreJ2 == "15")
                scoreJ2 = "30";
            else if (scoreJ2 == "30")
                scoreJ2 = "40";
            else if (scoreJ2 == "40"){
                if (scoreJ1 == "40")
                    scoreJ2 = "A";
                else if (scoreJ1 == "A")
                    scoreJ1 = "40";
                else
                    scoreJ2 = "GAGNE";
            }
            else if (scoreJ2 == "A")
                scoreJ2 = "GAGNE";
        }
        joueur = 0;
        if (!(scoreJ1 == "GAGNE" || scoreJ2 == "GAGNE"))
            std::cout << "score : " << scoreJ1 << " - " << scoreJ2 << std::endl;
    }
    if (scoreJ1 == "GAGNE")
        std::cout << "Le joueur 1 a gagné le jeu;" << std::endl;
    else
        std::cout << "Le joueur 2 a gagné le jeu;" << std::endl;
}

//  III

void bonjour(){
    std::string prenom , nom ;
    std::cout << "Quel est votre nom et votre prénom ? " << std::endl;
    std::cin >> nom >> prenom;
    
    for (int i = 0; i < nom.size(); i++) {
        nom[i] = toupper(nom[i]);
    }
    
    prenom[0] = toupper(prenom[0]);
    for (int i = 1; i < prenom.size(); i++) {
        prenom[i] = tolower(prenom[i]);
    }
    
    std::cout << "Bonjour " + prenom + " " + nom + " !" << std::endl;
}

//  IIII

void justeNombre(){
    int nb = rand()%1000;
    int saisie = 0;
    int essai = 0;
//    std::cout << nb << std::endl; // affiche le résultat pour mieux tester
    do {
        std::cout << "Saisissez un nombre entre 0 et 1000 : " << std::endl;
        std::cin >> saisie;
        essai ++;
        if (saisie > nb)
            std::cout << "Le nombre saisi est plus grand que le nombre à trouver." << std::endl;
        else if (saisie < nb)
            std::cout << "Le nombre saisi est plus petit que le nombre à trouver." << std::endl;
        else
            std::cout << "Félicitation ! Vous avez trouvé le bon nombre qui était " << nb << " en " << essai << " essais !" << std::endl;
    } while (saisie != nb);
}

void rechercheRandom(){
    int nb;
    int min = 0;
    int max = 1000;
    int coups = 0;
    int random;
    do {
        std::cout << "Saisissez un nombre un 0 et 1000 : " << std::endl;
        std::cin >> nb;
    } while (nb < 0 || nb > 1000);
    do {
        coups ++;
        random = min+rand()%max;
        if (random > nb)
            max = random;
        else if (random < nb)
            min = random;
    } while (random != nb);
    std::cout << "L'ordinateur a trouvé le nombre " << nb << " en " << coups << " coups." << std::endl;
}

void rechercheDichotomique(){
    int min = 0;
    int max = 1000;
    int coups = 0;
    int test;
    int nb;
    do {
        std::cout << "Saisissez un nombre un 0 et 1000 : " << std::endl;
        std::cin >> nb;
    } while (nb < 0 || nb > 1000);
    do {
        coups ++;
        test = ((min + max) / 2);
        if ((min+max)%2 != 0)
            test++; // arrondi supérieur
        if (test > nb)
            max = test;
        else if (test < nb)
            min = test;
    } while (test != nb);
    std::cout << "L'ordinateur a trouvé le nombre " << nb << " en " << coups << " coups." << std::endl;
}

//--------------------------------------------------------------------------------------------------------------------//

int main(int argc, const char * argv[]) {

    int a = 15;
    int b = 35;
    int c = 48;

//    I.1.1
    std::cout << " I.1.1 -------------------" << std::endl;

    std::cout << "a = " << a << std::endl;
    std::cout << "b = " << b << std::endl;
    std::cout << "Somme de a et b : " << somme(a,b) << std::endl;

//    I.1.2
    std::cout << " I.1.2 -------------------" << std::endl;

    inverse(a,b);
    std::cout << "a = " << a << std::endl;
    std::cout << "b = " << b << std::endl;

//    I.1.3-1
    std::cout << " I.1.3-1 -------------------" << std::endl;

    std::cout << "a = " << a << std::endl;
    std::cout << "b = " << b << std::endl;
    std::cout << sommePointeur(a, b, &c) << std::endl;
    std::cout << "c = " << c << std::endl;

//    I.1.3-2
    std::cout << " I.1.3-2 -------------------" << std::endl;

    std::cout << "a = " << a << std::endl;
    std::cout << "b = " << b << std::endl;
    std::cout << sommeReference(a, b, c) << std::endl;
    std::cout << "c = " << c << std::endl;

//    I.1.4
    std::cout << " I.1.4 -------------------" << std::endl;

    int tailleTableau;
    int sens;

    std::cout << "Taille du tableau : " << std::endl;
    std::cin >> tailleTableau ;

    std::cout << "Pour un tri croissant tapez 1, pour un tri décroissant tapez 2 : " << std::endl;
    std::cin >> sens;

    int tableau[tailleTableau];

    srand(time(0)); //vrai aléatoire (initialisation par rapport à la seconde actuelle)
    randomTab(tableau,tailleTableau);

    std::cout << "Tableau initial : " << std::endl;
    for (int i = 0; i < tailleTableau; i++) {
        std::cout << tableau[i] << std::endl;
    }
    std::cout << "----------------------------" << std::endl;

    orderTab(tableau, tailleTableau, sens);

//    II
    std::cout << " II -------------------" << std::endl;

    std::cout << tennis(3,3) << std::endl;
    match();

//    III
    std::cout << " III -------------------" << std::endl;

    bonjour();

//    IIII
    std::cout << " IIII -------------------" << std::endl;

    srand(time(0)); //vrai aléatoire (initialisation par rapport à la seconde actuelle)
    justeNombre();
    rechercheRandom();
    rechercheDichotomique();

    return 0;
}
