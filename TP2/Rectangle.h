//
//  Rectangle.hpp
//  TP2
//
//  Created by Laura Schaefer on 07/10/2020.
//  Copyright © 2020 Laura Schaefer. All rights reserved.
//

#ifndef Rectangle_hpp
#define Rectangle_hpp

#include <stdio.h>
#include "Point.h"

class Rectangle {
private:
    int longueur;
    int largeur;
    Point coinSupGauche;
    
public:
    Rectangle(int longueur, int largeur, Point p);
    
    void setLongeur(int l);
    int getLongeur();
    void setLargeur(int l);
    int getLargeur();
    void setCoinSupGauche(Point p);
    Point getCoinSupGauche();
    
    int Perimetre();
    int Surface();
    int PlusGrandPerimetreQue(Rectangle r);
    int PlusGrandeSurfaceQue(Rectangle r);
    void Afficher();
};

#endif /* Rectangle_hpp */
