//
//  Rectangle.cpp
//  TP2
//
//  Created by Laura Schaefer on 07/10/2020.
//  Copyright © 2020 Laura Schaefer. All rights reserved.
//
#include <iostream>
#include "Rectangle.h"
#include "Point.h"

    
    Rectangle::Rectangle(int _longueur, int _largeur, Point _p){
        longueur = _longueur;
        largeur = _largeur;
        coinSupGauche = _p;
    }
    
    void Rectangle::setLongeur(int l){
        longueur = l;
    }
    int Rectangle::getLongeur(){
        return longueur;
    }
    
    void Rectangle::setLargeur(int l){
        largeur = l;
    }
    int Rectangle::getLargeur(){
        return largeur;
    }
    
    void Rectangle::setCoinSupGauche(Point p){
        coinSupGauche = p;
    }
    Point Rectangle::getCoinSupGauche(){
        return coinSupGauche;
    }
    
    int Rectangle::Perimetre(){
        return (largeur + longueur) * 2;
    }
    
    int Rectangle::Surface(){
        return largeur * longueur;
    }
    
    int Rectangle::PlusGrandPerimetreQue(Rectangle r){
        if (this -> Perimetre() > r.Perimetre())
            return true;
        else
            return false;
    }
    
    int Rectangle::PlusGrandeSurfaceQue(Rectangle r){
        if (this -> Surface() > r.Surface())
            return true;
        else
            return false;
    }

void Rectangle::Afficher(){
    std::cout << "Longueur du rectangle : " << longueur << std::endl;
    std::cout << "Largeur du rectangle : " << largeur << std::endl;
    std::cout << "Coordonnées du coin supérieur gauche : (" << coinSupGauche.m_X << "," << coinSupGauche.m_Y << ")" << std::endl;
    std::cout << "Périmètre du rectangle : " << Perimetre() << std::endl;
    std::cout << "Surface du rectangle : " << Surface() << std::endl;
}
