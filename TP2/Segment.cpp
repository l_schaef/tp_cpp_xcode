//
//  Segment.cpp
//  TP2
//
//  Created by Laura Schaefer on 09/10/2020.
//  Copyright © 2020 Laura Schaefer. All rights reserved.
//
#include <iostream>
#include "Segment.h"
#include "Point.h"
#include <stdlib.h>
#include <cmath>

Segment::Segment(Point _p1, Point _p2){
    p1 = _p1;
    p2 = _p2;
};

Segment::Segment(){};

void Segment::setP1(Point p){
    p1 = p;
}
Point Segment::getP1(){
    return p1;
}

void Segment::setP2(Point p){
    p2 = p;
}
Point Segment::getP2(){
    return p2;
}
        
double Segment::Longueur(){
    
    double aCarre = (abs(p1.m_X - p2.m_X))*(abs(p1.m_X - p2.m_X));
    double bCarre = (abs(p1.m_Y - p2.m_Y))*(abs(p1.m_Y - p2.m_Y));
    double cCarre = aCarre + bCarre;
    double c = sqrt(cCarre);
    
    return c;
}

void Segment::Afficher(){
    std::cout << "Segment : [(" << p1.m_X << "," << p1.m_Y << ") (" << p2.m_X << "," << p2.m_Y << ")]" << std::endl;
    std::cout << "Longueur du segment : " << Longueur() << std::endl;
}

