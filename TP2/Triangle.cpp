//
//  Triangle.cpp
//  TP2
//
//  Created by Laura Schaefer on 07/10/2020.
//  Copyright © 2020 Laura Schaefer. All rights reserved.
//

#include <iostream>
#include "Triangle.h"
#include "Point.h"
#include "Segment.h"
#include <cmath>
#include <stdlib.h>

    Triangle::Triangle(Point _p1, Point _p2, Point _p3){
        p1 = _p1;
        p2 = _p2;
        p3 = _p3;
        s1 = Segment(_p1, _p2);
        s2 = Segment(_p2, _p3);
        s3 = Segment(_p1, _p3);
    }
    
    void Triangle::setP1(Point p){
        p1 = p;
    }
    Point Triangle::getP1(){
        return p1;
    }
    
    void Triangle::setP2(Point p){
        p2 = p;
    }
    Point Triangle::getP2(){
        return p2;
    }
    
    void Triangle::setP3(Point p){
        p3 = p;
    }
    Point Triangle::getP3(){
        return p3;
    }

    void Triangle::setS1(Segment s){
        s1 = s;
    }
    Segment Triangle::getS1(){
        return s1;
    }

    void Triangle::setS2(Segment s){
        s2 = s;
    }
    Segment Triangle::getS2(){
        return s2;
    }

    void Triangle::setS3(Segment s){
        s3 = s;
    }
    Segment Triangle::getS3(){
        return s3;
    }

Segment Triangle::Base(){
    if (s1.Longueur() > s2.Longueur() && s1.Longueur() > s3.Longueur())
        return s1;
    else if (s2.Longueur() > s1.Longueur() && s2.Longueur() > s3.Longueur())
        return s2;
    else
         return s3;
};

double Triangle::Surface(){
    double a = s1.Longueur();
    double b = s2.Longueur();
    double c = s3.Longueur();
    double surface = sqrt((( a + b + c ) / 2)*(( a + b + c ) / 2 - a)*(( a + b + c ) / 2 - b)*(( a + b + c ) / 2 - c));
    return surface;
};


double Triangle::Hauteur(){
    return 2*Surface()/Base().Longueur();
};

bool Triangle::Isocele(){
    if (s1.Longueur() == s2.Longueur())
        return true;
    else if (s1.Longueur() == s3.Longueur())
        return true;
    else if (s2.Longueur() == s3.Longueur())
        return true;
    else
        return false;
};

bool Triangle::Rectangle(){
    double aCarre = pow(s1.Longueur(), 2);
    double bCarre = pow(s2.Longueur(), 2);;
    double cCarre = pow(s3.Longueur(), 2);;
    
    if ( (aCarre+bCarre) == cCarre || (aCarre+cCarre) == bCarre || (bCarre+cCarre == aCarre))
        return true;
    else
        return false;
};

bool Triangle::Equilateral(){
    if (s1.Longueur() == s2.Longueur() && s1.Longueur() == s3.Longueur() && s2.Longueur() == s3.Longueur())
        return true;
    else
        return false;
};

void Triangle::Afficher(){
    std::cout << "Coordonnées des points du triangle : (" << p1.m_X << "," << p1.m_Y << ") (" << p2.m_X << "," << p2.m_Y << ") (" << p3.m_X << "," << p3.m_Y << ")" << std::endl;
    std::cout << "Longueurs des côtés du triangle : " << s1.Longueur() << " - " << s2.Longueur() << " - " << s3.Longueur() << std::endl;
    std::cout << "Base du triangle : [(" << Base().getP1().m_X << "," << Base().getP1().m_Y << ") (" << Base().getP2().m_X << "," << Base().getP2().m_Y << ")]" << std::endl;
    std::cout << "Hauteur du triangle : " << Hauteur() << std::endl;
    std::cout << "Surface du triangle : " << Surface() << std::endl;
    if (Equilateral()) {
        std::cout << "Ce triangle est équilatéral" << std::endl;
    }
    else if (Isocele()) {
        std::cout << "Ce triangle est isocèle" << std::endl;
    }
    if (Rectangle()) {
        std::cout << "C'est un triangle rectangle." << std::endl;
    }
}
