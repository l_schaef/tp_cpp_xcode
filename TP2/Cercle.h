//
//  Cercle.hpp
//  TP2
//
//  Created by Laura Schaefer on 11/10/2020.
//  Copyright © 2020 Laura Schaefer. All rights reserved.
//

#ifndef Cercle_hpp
#define Cercle_hpp

#include <stdio.h>
#include "Point.h"

class Cercle {
  
    private :
    
    Point centre;
    int diametre;
    
    public :
    
    Cercle(Point centre, int diametre);
    
    void setCentre(Point p);
    Point getCentre();
    void setDiametre(int d);
    int getDiametre();
    
    double Perimetre();
    double Surface();
    bool SurLeCercle(Point p);
    bool DansLeCercle(Point p);
    void Afficher();
};


#endif /* Cercle_hpp */
