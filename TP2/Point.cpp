//
//  Point.cpp
//  TP2
//
//  Created by Laura Schaefer on 07/10/2020.
//  Copyright © 2020 Laura Schaefer. All rights reserved.
//
#include <iostream>
#include "Point.h"

Point::Point(){
    m_X = 0;
    m_Y = 0;
}

Point::Point(double _x, double _y){
    m_X = _x;
    m_Y = _y;
}

void Point::Afficher(){
    std::cout << "Les coordonnées du point : (" << m_X << "," << m_Y << ")" << std::endl;
};
