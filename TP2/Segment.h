//
//  Segment.hpp
//  TP2
//
//  Created by Laura Schaefer on 09/10/2020.
//  Copyright © 2020 Laura Schaefer. All rights reserved.
//

#ifndef Segment_hpp
#define Segment_hpp

#include <stdio.h>
#include "Point.h"

class Segment {
    private:
        Point p1;
        Point p2;
        
    public:
        Segment(Point _p1, Point _p2);
        Segment();
    
        void setP1(Point p);
        Point getP1();
        void setP2(Point p);
        Point getP2();
        
        double Longueur();
        void Afficher();
};

#endif /* Segment_hpp */
