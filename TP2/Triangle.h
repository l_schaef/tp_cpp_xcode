//
//  Triangle.hpp
//  TP2
//
//  Created by Laura Schaefer on 07/10/2020.
//  Copyright © 2020 Laura Schaefer. All rights reserved.
//

#ifndef Triangle_hpp
#define Triangle_hpp

#include <stdio.h>
#include <iostream>
#include "Point.h"
#include "Segment.h"

class Triangle {
    
private:
    Point p1;
    Point p2;
    Point p3;
    Segment s1;
    Segment s2;
    Segment s3;
    
public:
    Triangle(Point _p1, Point _p2, Point _p3);
    
    void setP1(Point p);
    Point getP1();
    void setP2(Point p);
    Point getP2();
    void setP3(Point p);
    Point getP3();
    void setS1(Segment s);
    Segment getS1();
    void setS2(Segment s);
    Segment getS2();
    void setS3(Segment s);
    Segment getS3();
    
    Segment Base();
    double Hauteur();
    double Surface();
    bool Isocele();
    bool Rectangle();
    bool Equilateral();
    void Afficher();
};

    


#endif /* Triangle_hpp */
