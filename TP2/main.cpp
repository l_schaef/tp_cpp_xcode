//
//  main.cpp
//  TP2
//
//  Created by Laura Schaefer on 07/10/2020.
//  Copyright © 2020 Laura Schaefer. All rights reserved.
//

#include <iostream>
#include "Point.h"
#include "Rectangle.h"
#include "Triangle.h"
#include "Segment.h"
#include "Triangle.h"
#include "Cercle.h"

int main(int argc, const char * argv[]) {
    Point point{2,3};
    point.Afficher();
    
    std::cout << std::endl;
    
    Rectangle r1 { 2, 10, Point { 1, 2}};
    r1.Afficher();

    Rectangle r2 { 15, 5, Point { 1, 2}};
    std::string p = r1.PlusGrandPerimetreQue(r2) ? "Oui" : "Non";
    std::cout << "Périmètre du rectangle 1 supérieur à celui du rectangle 2 ? : " << p << std::endl;
    std::string s = r2.PlusGrandeSurfaceQue(r1) ? "Oui" : "Non";
    std::cout << "Surface du rectangle 2 supérieure à celle du rectangle 1 ? : " << s << std::endl;
    
    std::cout << std::endl;

    Triangle t1 {Point { 4, 2}, Point { 9, 1}, Point { 5, 4}}; //Quelconque
    Triangle t3 {Point { 3, 2}, Point { 7, 2}, Point { 5, 7}}; //Isocèle
    Triangle t4 {Point { 1, 1}, Point { 5, 1}, Point { 5, 4}}; //Rectangle

    t1.Afficher();
    
    std::cout << std::endl;
    
    t3.Afficher();
    
    std::cout << std::endl;
    
    t4.Afficher();
    
    //Un triangle ne peut pas être équilatéral avec 3 coordonnées entières
    
    std::cout << std::endl;

    Cercle c{Point{2 , 2}, 3};
    c.Afficher();
    
    Point p1 {4 , 4};
    std::string surLeCercle1 = c.SurLeCercle(p1) ? "Oui" : "Non";
    std::cout << "Le point 1 est-il sur le cercle ? : " << surLeCercle1 << std::endl;
    std::string dansLeCercle1 = c.DansLeCercle(p1) ? "Oui" : "Non";
    std::cout << "Le point 1 est-il dans le cercle ? : " << dansLeCercle1 << std::endl;
    
    Point p2 {1 , 3};
    std::string surLeCercle2 = c.SurLeCercle(p2) ? "Oui" : "Non";
    std::cout << "Le point 2 est-il sur le cercle ? : " << surLeCercle2 << std::endl;
    std::string dansLeCercle2 = c.DansLeCercle(p2) ? "Oui" : "Non";
    std::cout << "Le point 2 est-il dans le cercle ? : " << dansLeCercle2 << std::endl;
    
    Point p3 {2 , 0.5};
    std::string surLeCercle3 = c.SurLeCercle(p3) ? "Oui" : "Non";
    std::cout << "Le point 3 est-il sur le cercle ? : " << surLeCercle3 << std::endl;
    std::string dansLeCercle3 = c.DansLeCercle(p3) ? "Oui" : "Non";
    std::cout << "Le point 3 est-il dans le cercle ? : " << dansLeCercle3 << std::endl;
    
    return 0;
}
