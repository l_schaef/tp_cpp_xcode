//
//  Cercle.cpp
//  TP2
//
//  Created by Laura Schaefer on 11/10/2020.
//  Copyright © 2020 Laura Schaefer. All rights reserved.
//

#include <iostream>
#include "Cercle.h"
#include "Point.h"
#include <math.h>
#include "Segment.h"

Cercle::Cercle(Point _centre, int _diametre){
    centre = _centre;
    diametre = _diametre;
};

void Cercle::setCentre(Point p){
    centre = p;
}
Point Cercle::getCentre(){
    return centre;
}

void Cercle::setDiametre(int d){
    diametre = d;
}
int Cercle::getDiametre(){
    return diametre;
}

double Cercle::Perimetre(){
    return M_PI * diametre;
}

double Cercle::Surface(){
    return M_PI * pow(diametre, 2)/4;
}

bool Cercle::SurLeCercle(Point p){
    double distance = Segment{centre, p}.Longueur();
    double rayon = (double)diametre/2;
    if (distance == rayon)
        return true;
    else
        return false;
}

bool Cercle::DansLeCercle(Point p){
    double distance = Segment{centre, p}.Longueur();
    double rayon = (double)diametre/2;
    if (distance < rayon)
        return true;
    else
        return false;
}

void Cercle::Afficher(){
    std::cout << "Centre du cercle : (" << centre.m_X << "," << centre.m_Y << ")" << std::endl;
    std::cout << "Diamètre du cercle : " << diametre << std::endl;
    std::cout << "Périmètre du cercle : " << Perimetre() << std::endl;
    std::cout << "Surface du cercle : " << Surface() << std::endl;
}
