//
//  Point.hpp
//  TP2
//
//  Created by Laura Schaefer on 07/10/2020.
//  Copyright © 2020 Laura Schaefer. All rights reserved.
//

#ifndef Point_hpp
#define Point_hpp

#include <stdio.h>

struct Point {
    Point();
    Point(double x, double y);
    double m_X;
    double m_Y;
    void Afficher();
};

#endif /* Point_hpp */
